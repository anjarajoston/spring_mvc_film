<%-- 
    Document   : index
    Created on : 31 janv. 2023, 18:09:01
    Author     : User_HP
--%>
<%@page import="model.Film"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Film> list = (ArrayList) request.getAttribute("list");
    Vector genre = (Vector) request.getAttribute("genre");
    int nbrPage = (Integer) request.getAttribute("nbrPage");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
      <div class="container"><a class="navbar-brand logo" href="#">Home </a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navcol-1">
          <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <form class="navbar-form navbar-right" action="<%=request.getContextPath()%>/recherche" method="Post">
                        <input type="text" id="search-field" name="motcle" placeholder="mot clet">
                        <button type="submit">Rechercher</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form class="navbar-form navbar-right" action="<%=request.getContextPath()%>/parPage" method="Post">
                        <input type="text" id="search-field" name="parPage" placeholder="nombre par page">
                        <button type="submit">Valider</button>
                    </form>
                </li>
          </ul>
        </div>
      </div>
    </nav>
<main class="page contact-us-page">
  <section class="clean-block clean-form dark">
    <div class="container">
      <div class="block-heading">
        <div class="row justify-content-center">
            <% for (int i = 0 ; i < list.size() ; i++ ) { %>
            <div class="col-sm-6 col-lg-4">
                <div class="card text-center clean-card">
                    <a href="<%=request.getContextPath()%>/details?idFilm=<%= list.get(i).getId()%>"><img class="card-img-top w-100 d-block" src="${pageContext.request.contextPath}/resources/assets/img/<%= list.get(i).getVisuel()%>"></a>
                    <div class="card-body info">
                        <p class="card-text">Titre : <%= list.get(i).getTitre()%></p>
                        <p class="card-text">Auteur : <%= list.get(i).getAuteur()%></p>
                        <p class="card-text">Genre : <%= genre.elementAt(i)%></p>
                        <p class="card-text"><a class="nav-link" href="<%=request.getContextPath()%>/ajout?idFilm=<%= list.get(i).getId()%>">Ajout scène</a></p>
                        <p class="card-text"><a class="nav-link" href="<%=request.getContextPath()%>/planning?idfilm=<%= list.get(i).getId()%>&&jour=1">PLanning</a></p>
                        <div class="icons"><a href="#"><i class="icon-social-facebook"></i></a><a href="#"><i class="icon-social-instagram"></i></a><a href="#"><i class="icon-social-twitter"></i></a></div>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
      </div>
    </div>
    <nav>
        <ul class="pagination">
            <% if( request.getAttribute("recherche")==null ){ %>
            <% for(int i = 0; i < nbrPage; i++){ %>
                <li class="page-item">
                <a href="<%= request.getContextPath() %>/pagination?num=<%= i %>" class="page-link"><%= i+1 %></a>
                </li>
            <% } %>
            <% } %>
        </ul>
    </nav>
  </section>
</main>
<footer class="page-footer dark">
  <div class="container">
  </div>
  <div class="footer-copyright">
    <p>© 2023 Copyright Text</p>
  </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
</body>
</html>


<%-- 
    Document   : details
    Created on : 1 mars 2023, 12:11:17
    Author     : User_HP
--%>
<%@page import="model.Film"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Vi_scene"%>
<%
    ArrayList<Vi_scene> details = (ArrayList) request.getAttribute("details");
    Film film = (Film) request.getAttribute("film");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Details</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
      <div class="container"><a class="navbar-brand logo" href="#">Détails</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navcol-1">
          <ul class="navbar-nav ms-auto">
            <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/">Home</a></li>
          </ul>
        </div>
      </div>
    </nav>
<main class="page contact-us-page">
  <section class="clean-block clean-form dark">
    <div class="container">
      <div class="block-heading">
        <div class="row justify-content-center">
            <h1 class="text-info">Film</h1>
            <div class="col-sm-6 col-lg-4">
                <div class="card text-center clean-card">
                    <img class="card-img-top w-100 d-block" src="${pageContext.request.contextPath}/resources/assets/img/<%= film.getVisuel()%>">
                    <div class="card-body info">
                        <p class="card-text">Titre : <%= film.getTitre()%></p>
                        <p class="card-text">Auteur : <%= film.getAuteur()%></p>
                        <p class="card-text"><a class="nav-link" href="<%=request.getContextPath()%>/ajout?idFilm=<%= film.getId()%>">Ajout scène</a></p>
                        <div class="icons"><a href="#"><i class="icon-social-facebook"></i></a><a href="#"><i class="icon-social-instagram"></i></a><a href="#"><i class="icon-social-twitter"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="block-heading">
        <div class="row justify-content-center">
            <% for (int i = 0 ; i < details.size() ; i++ ) { %>
            <h1 class="text-info">Scène <%=i+1%></h1>
            <div class="col-sm-6 col-lg-4">
                <div class="card text-center clean-card">
                    <div class="card-body info">
                        <p class="card-text">Titre : <%= details.get(i).getTitre()%></p>
                        <p class="card-text">Type scène : <%= details.get(i).getTypescene()%></p>
                        <p class="card-text">Plateau : <%= details.get(i).getPlateau()%></p>
                        <p class="card-text">Durée : <%= details.get(i).getDuree()%></p>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
      </div>
    </div>
    <nav>
        <ul class="pagination">
            
        </ul>
    </nav>
  </section>
</main>
<footer class="page-footer dark">
  <div class="container">
  </div>
  <div class="footer-copyright">
    <p>© 2023 Copyright Text</p>
  </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
    </body>
</html>

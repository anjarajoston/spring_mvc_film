
<%@page import="model.Planning"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Planning> planning = (ArrayList<Planning>) (request.getAttribute("plan"));
    double nbrJour = (Double) (request.getAttribute("nbrJour"));
    int idfilm=(Integer) (request.getAttribute("idfilm"));
%>

<!DOCTYPE html>
<html>
    <head>
        <title>Planning</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container"><a class="navbar-brand logo" href="#">Planning</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
              <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/">Home</a></li>
                </ul>
              </div>
            </div>
        </nav>
<br>
<br>
<br>
    <div class="card-header my-3">Les planning des scene</div>
    <% for (int i = 0; i < planning.size(); i++) {%>
    <div class="container">
        <div class="card-header-mx-3"></div>
        <div class="row">
            <div class="col-md-3">
                <div class="card w-100" style="width:25rem; border-radius:20px ">
                    <img style="border-radius: 20px" class="Card-img-top"
                    <div class="col-md-3">
                    <div class="card-body">
                        <h2 <b class="card-title" style="color: darkred;font-family: Lucida Bright"><% out.print(planning.get(i).getTitre());%></b></h2>
                        <p class="" style="font-family: Alef">Auteur : <% out.print(planning.get(i).getAuteur());%></p>
                        <h6 class="category" style="color: darkslategray;font-family: Alef">Plateau : <% out.print(planning.get(i).getPlateau());%></h6>
                        <h6 class="category" style="color:green;font-family:Alef">Dure: <% out.print(planning.get(i).getHeure());%> heur</h6>
                        <div class="mt-3 d-flex justify-content-between"></div>
                    </div>
                </div>
            </div>	
    <% } %>
<br>
<br>
        <main class="page contact-us-page">
            <section class="clean-block clean-form dark">
                <nav>
                    <ul class="pagination">
                        <h4>Nombre de jour : </h4>
                        <% for (int i = 0; i < nbrJour; i++) { %>
                            <li class="page-item"><a class="page-link" href="planning?idfilm=<% out.print(idfilm);%>&&jour=<% out.print(i + 1);%>"><% out.print(i + 1);%></a></li>
                        <% }%>
                    </ul>
                </nav>
            </section>
        </main>
        <footer class="page-footer dark">
            <div class="container">
            </div>
            <div class="footer-copyright">
              <p>� 2023 Copyright Text</p>
            </div>
        </footer>
        <script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
    </body>
</html>
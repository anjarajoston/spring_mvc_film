<%-- 
    Document   : ajout
    Created on : 28 févr. 2023, 21:45:14
    Author     : User_HP
--%>
<%@page import="model.Plateau"%>
<%@page import="model.TypeScene"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<TypeScene> typesScenes = (ArrayList) request.getAttribute("typesScenes");
    ArrayList<Plateau> plateaux = (ArrayList) request.getAttribute("plateaux");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Ajout</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container"><a class="navbar-brand logo" href="#">Ajout</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
              <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/">Home</a></li>
                </ul>
              </div>
            </div>
        </nav>
        <main class="page contact-us-page">
            <section class="clean-block clean-form dark">
              <div class="container">
                <div class="block-heading">
                    <div class="container">
                        <div class="block-heading">
                            <h1 class="text-info">Ajout scène</h1>
                        </div>
                        <form action="<%=request.getContextPath()%>/addScene" method="post">
                            <input type="hidden" value="<%=request.getAttribute("idFilm")%>" name="idFilm">
                            <div class="mb-3">
                                <label class="form-label" for="type">Type scène :</label>
                                <select class="form-control" name="type" id="type">
                                    <% for (TypeScene typeScene : typesScenes) { %>
                                    <option value="<%= typeScene.getId() %>"><%= typeScene.getTypeScene() %></option>
                                    <% } %>
                                </select> 
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="nombre">Nombre acteur :</label>
                                <input class="form-control" type="number" name="nombre" id="nombre" onchange="getActeur(this.value)">
                            </div>
                            <div class="mb-3" id="personnage">
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="plateau">Plateau :</label>
                                <select class="form-control" name="plateau" id="plateau">
                                    <% for (Plateau plateau : plateaux) { %>
                                    <option value="<%= plateau.getId() %>"><%= plateau.getPlateau() %></option>
                                    <% } %>
                                </select> 
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="duree">Durée :</label>
                                <input class="form-control" type="time" name="duree" id="duree">
                            </div>
                            <div class="mb-3">
                                <button class="btn btn-primary" type="submit">Ajouter</button>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </section>
         </main>
        <footer class="page-footer dark">
            <div class="container">
            </div>
            <div class="footer-copyright">
              <p>© 2023 Copyright Text</p>
            </div>
        </footer>
        <script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
        <script>
            function getActeur(nombre) {
                //alert(nombre)
                var xhr; 
                try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
                catch (e) 
                {
                    try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                    catch (e2) 
                    {
                        try {  xhr = new XMLHttpRequest();  }
                        catch (e3) {  xhr = false;   }
                    }
                }
                xhr.onreadystatechange  = function(){ 
                    if(xhr.readyState  === 4){
                        if(xhr.status  === 200){
                            var text = this.responseText;
                            //alert(text);
                            personnage.innerHTML = text;
                        }
                    }
                };
                xhr.open("GET","<%=request.getContextPath()%>/getActeur?nombre="+nombre,  true); 
                xhr.send(null);
            }
        </script>
    </body>
</html>

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  User_HP
 * Created: 28 févr. 2023
 */

CREATE DATABASE film;
CREATE ROLE film LOGIN PASSWORD 'film';
ALTER DATABASE film OWNER TO film;
\c film film;

CREATE TABLE plateau(
    id serial PRIMARY KEY,
    plateau VARCHAR(50)
);
insert into plateau (plateau) values ('maison'),('eglise'),('rue');
CREATE TABLE typeScene(
    id serial PRIMARY KEY,
    typeScene VARCHAR(50)
);

insert into typeScene (typeScene) values ('dialogue'),('voix off');

CREATE TABLE film(
    id serial PRIMARY KEY,
    visuelle   VARCHAR(50),
    titre VARCHAR(50),
    auteur VARCHAR(50)
);

insert into film (visuelle,titre,auteur)
values
('Malokila 15','Malokila 15','Rajao'),
('Baraingo','Baraingo','Pasitera');

CREATE TABLE scene(
    id serial PRIMARY KEY,
    idFilm int references film(id),
    idType int references typeScene(id),
    idPlateau int references plateau(id),
    duree TIME
);
insert into scene (idFilm,idType,idPlateau,duree) values(1,1,1,'1:30:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,2,1,'0:20:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,2,2,'0:15:30');
insert into scene (idFilm,idType,idPlateau,duree) values(2,1,2,'0:15:30');

insert into scene (idFilm,idType,idPlateau,duree) values(1,1,1,'0:05:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,2,1,'0:25:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,2,2,'0:15:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,1,1,'0:30:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,1,1,'0:20:30');
insert into scene (idFilm,idType,idPlateau,duree) values(1,2,2,'0:15:30');
CREATE TABLE acteur(
    id serial PRIMARY KEY,
    nom VARCHAR(50),
    role VARCHAR(50)
);

CREATE TABLE typeEmotion(
    id serial PRIMARY KEY,
    type VARCHAR(50),
    dureEstimer TIME
);

CREATE TABLE personnageAction(
    idScene int references scene(id),
    idActeur int references acteur(id),
    idEmotion int references typeEmotion(id),
    phrase TEXT,
    duree TIME
);

CREATE TABLE estimation(
    idScene int references scene(id),
    idPlateau int references plateau(id),
    duree TIME
 );

alter table estimation add column idfilm int references film(id);

insert into estimation(idscene,idplateau,duree)
values

(1,1,'3:30:00'),
(2,1,'2:00:00'),
(3,2,'1:30:00'),
(4,1,'00:30:00'),
(5,1,'01:00:00'),
(6,2,'00:30:00'),
(7,1,'01:30:00'),
(8,1,'02:00:00'),
(9,2,'03:20:00');



select sum(est.duree) as heure as duree from estimation 
est join scene sc on est.idscene=sc.id where idfilm=1;

create or replace view V_Estimation as (
 select film.*,pl.plateau,est.* from estimation est 
 join film on est.idfilm=film.id
 join plateau pl on est.idplateau=pl.id
);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ObjetBdd;
import java.sql.Time;

/**
 *
 * @author User_HP
 */
public class TypeEmotion extends ObjetBdd{
    int id;
    String type;
    int dureEstimer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDureEstimer() {
        return dureEstimer;
    }

    public void setDureEstimer(int dureEstimer) {
        this.dureEstimer = dureEstimer;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ObjetBdd;

/**
 *
 * @author User_HP
 */
public class Pagination extends ObjetBdd{
    int id;
    int pagination;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPagination() {
        return pagination;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }
    
    public int getNbrPage()throws Exception {
        int size = new Film().find(null).size();
        int nbrPage = 0;
        try {
            int pagination = ((Pagination)new Pagination().find(null).get(0)).getPagination();
            if( size%pagination == 0 ){
                nbrPage = size/pagination;
            } else {
                if( (int)size/pagination == 0 ){
                    nbrPage = 0;
                } else {
                    nbrPage = (size/pagination)+1;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return nbrPage;
    }
}

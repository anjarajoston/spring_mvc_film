/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import AllAnnotation.NOTPRIS;
import connection.ConnectionDB;
import genreiquedao.GeneriqueDAO;

import java.sql.Connection;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Planning {

    @NOTPRIS
    int idfilm;

    @NOTPRIS
    int idscene;

    @NOTPRIS
    int jour;

    double heure;

    @NOTPRIS
    Time heureFin;

    @NOTPRIS
    String visuelle;

    @NOTPRIS
    String titre;

    @NOTPRIS
    String auteur;

    public String getPlateau() {
        return plateau;
    }

    public void setPlateau(String plateau) {
        this.plateau = plateau;
    }

    @NOTPRIS
    String plateau;

    public String getVisuelle() {
        return visuelle;
    }

    public void setVisuelle(String visuelle) {
        this.visuelle = visuelle;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }

    public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }

    public double getHeure() {
        return heure;
    }

    public void setHeure(double heure) {
        this.heure = heure;
    }

    public Time getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Time heureFin) {
        this.heureFin = heureFin;
    }

    public double getNombreJour(int idfilm, Connection con, GeneriqueDAO dao) {
        try {
            double nbrejour = 0;
            System.out.println("==>" + "select sum(est.duree) as heure from estimation \n"
                    + "est join scene sc on est.idscene=sc.id where est.idfilm=" + idfilm);;
            ArrayList<Planning> plan = dao.ExecuteRequete(new Planning(), "select sum(est.duree) as heure from estimation \n"
                    + "est join scene sc on est.idscene=sc.id where est.idfilm=" + idfilm, con);
            if (plan.size() != 0) {
                Planning pl = plan.get(0);
                /*Time tm = pl.getHeure();
                int hours = Integer.parseInt(tm.toString().split(":")[0]);
                int min = Integer.parseInt(tm.toString().split(":")[1]) / 60;
                int sec = Integer.parseInt(tm.toString().split(":")[2]) / 3600;*/
                double jour = pl.getHeure();
                if (jour % 8 == 0) {
                    return jour / 8;
                } else {
                    return ((int) (jour / 8)) + 1;
                }
            }
            return nbrejour;
        } catch (Exception ex) {
            Logger.getLogger(Planning.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public ArrayList<Planning> getPlanning(Connection con, GeneriqueDAO dao) {
        ArrayList<Planning> planning = new ArrayList<Planning>();

        try {
            Estimation est = new Estimation();
            est.setIdfilm(this.getIdfilm());
            ArrayList<V_Estimation> estime = dao.ExecuteRequete(new V_Estimation(), "select * from v_estimation where idfilm=" + est.getIdfilm() + " order by idplateau", con);
            double somme = 0;
            double jourNB = getNombreJour(est.getIdfilm(), con, dao);
            int jour = 1;
            System.out.println(estime.size() + "model.Planning.getPlanning()" + "select * from v_estimation where idfilm=" + est.getIdfilm() + " order by idplateau");
            for (int i = 0; i < estime.size(); i++) {
                somme += estime.get(i).getDuree();
                ///for (int n = 0; n < (int) (estime.get(i).getDuree() / 8); n++) {
                    Planning plan = new Planning();
                    if (somme <= 8) {
                        plan.setJour(jour);
                    }
                    if (somme > 8) {
                        plan.setJour(jour + 1);
                    }
                    plan.setIdscene(estime.get(i).getIdscene());
                    plan.setHeure(estime.get(i).getDuree());
                    plan.setVisuelle(estime.get(i).getVisuel());
                    plan.setTitre(estime.get(i).getTitre());
                    plan.setAuteur(estime.get(i).getAuteur());
                    plan.setPlateau(estime.get(i).getPlateau());
                    planning.add(plan);
                ////}

            }
            System.out.println("===>" + planning.get(0).getJour());

        } catch (Exception ex) {
            Logger.getLogger(Planning.class.getName()).log(Level.SEVERE, null, ex);
        }
        return planning;
    }

    public ArrayList<Planning> getPlanningJour(ArrayList<Planning> plan, int jour) {
        ArrayList<Planning> plani = new ArrayList<Planning>();
        for (int i = 0; i < plan.size(); i++) {
            if (plan.get(i).getJour() == jour) {
                plani.add(plan.get(i));
            }
        }
        return plani;
    }

    public double SommeTime(Time tm) {
        double hours = Double.parseDouble(tm.toString().split(":")[0]);
        double min = Double.parseDouble(tm.toString().split(":")[1]) / 60;
        double sec = Double.parseDouble(tm.toString().split(":")[2]) / 3600;
        return hours + min + sec;
    }

    public static void main(String[] str) {
        try {
            Planning plan = new Planning();
            plan.setIdfilm(2);
            Connection con = new ConnectionDB().getConnection("postgres");
            ///System.out.println("===> Nombre Jour " + plan.getNombreJour(1, con, new GeneriqueDAO()));
            ArrayList<Planning> planii = plan.getPlanningJour(plan.getPlanning(con, new GeneriqueDAO()),1);

            for (int i = 0; i < planii.size(); i++) {
                System.out.println(planii.get(i).getPlateau() + "=== " + planii.get(i).getJour() + " ===> Nombre Jour " + planii.get(i).getIdscene() + " titre =" + planii.get(i).getJour() + " Visuelle = " + planii.get(i).getVisuelle() + " Auteur = > " + planii.get(i).getAuteur());
            }
        } catch (Exception ex) {
            Logger.getLogger(Planning.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Time;

/**
 *
 * @author ASUS
 */
public class Estimation {
    
    int idscene;
    int idplateau;
    Time duree;
    int idfilm;

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }
    
    
    

    public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idpalateau) {
        this.idplateau = idpalateau;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time dure) {
        this.duree = dure;
    }
    
    
    
}

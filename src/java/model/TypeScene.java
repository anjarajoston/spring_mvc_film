/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ObjetBdd;

/**
 *
 * @author User_HP
 */
public class TypeScene extends ObjetBdd{
    int id;
    String typeScene;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeScene() {
        return typeScene;
    }

    public void setTypeScene(String typeScene) {
        this.typeScene = typeScene;
    }
}

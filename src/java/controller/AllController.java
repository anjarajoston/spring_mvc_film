/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import connection.ConnectionDB;
import dao.DaoGeneric;
import dao.HibernateDao;
import genreiquedao.GeneriqueDAO;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Acteur;
import model.Film;
import model.Genrefilm;
import model.Pagination;
import model.PersonnageAction;
import model.Planning;
import model.Plateau;
import model.Scene;
import model.TypeEmotion;
import model.TypeScene;
import model.Vi_scene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AllController {
    @Autowired
    HibernateDao dao;
    
    @RequestMapping(value="/")
    public String index(Model model){
        DaoGeneric daogeneric = new DaoGeneric();
        ArrayList list = null;
        ArrayList listgenre = null;
        Vector genre = new Vector();
        Pagination page = new Pagination();
        try {
            String aa="";
            Pagination pagination = (Pagination)page.find(null).get(0);
            list = daogeneric.find("select * from film order by id asc limit "+pagination.getPagination(), new Film());
            for(int i = 0 ; i < list.size() ; i++){
                listgenre = daogeneric.find("select * from genrefilm where id = "+((Film) list.get(i)).getId(), new Genrefilm());
                if(listgenre.size()!=0){
                    aa = ((Genrefilm) listgenre.get(0)).getIntitule();
                }
                
                for(int j = 1 ; j < listgenre.size() ; j++){
                    aa+=","+((Genrefilm) listgenre.get(j)).getIntitule();
                }
                genre.add(aa);
            }
            model.addAttribute("nbrPage",page.getNbrPage());
            model.addAttribute("list",list);
            model.addAttribute("genre",genre);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }
    
    @RequestMapping(value="/ajout")
    public String ajout(Model model, HttpServletRequest request){
        try {
            model.addAttribute("idFilm", request.getParameter("idFilm"));
            model.addAttribute("typesScenes", new TypeScene().find(null));
            model.addAttribute("plateaux",new Plateau().find(null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ajout";
    }
            
    @RequestMapping(value="/getActeur")
    public void getActeur(Model model, HttpServletRequest request, HttpServletResponse response){
        try {
            PrintWriter out = response.getWriter();
            ArrayList<Object> acteurs = new Acteur().find(null);
            ArrayList<Object> emotions = new TypeEmotion().find(null);
            int nombre = Integer.parseInt(request.getParameter("nombre"));
            String form = "";
            for (int i = 0; i < nombre; i++) {
                form += "<div class=\"mb-3\">";
                form += "<label class=\"form-label\" for=\"acteur\">Acteur "+(i+1)+" :</label>";
                form += "<select class=\"form-control\" name=\"acteur_"+(i+1)+"\" id=\"acteur\">";
                for (Object acteur : acteurs) { 
                    form += "<option value="+((Acteur)acteur).getId() +">"+((Acteur)acteur).getNom()+"</option>\n";
                }
                form += "</select> ";
                form += "<label class=\"form-label\" for=\"mot\">Mot :</label>\n";
                form += "<input class=\"form-control\" type=\"text\" name=\"mot_"+(i+1)+"\" id=\"mot\" \"> ";
                form += "<label class=\"form-label\" for=\"emotion\">Emotion :</label>";
                form += "<select class=\"form-control\" name=\"emotion_"+(i+1)+"\" id=\"emotion\">";
                for (Object emotion : emotions) { 
                    form += "<option value="+((TypeEmotion)emotion).getId() +">"+((TypeEmotion)emotion).getType()+"</option>\n";
                }
                form += "</select> ";
                form += "<label class=\"form-label\" for=\"speach\">Duree speach :</label>\n";
                form += "<input class=\"form-control\" type=\"time\" name=\"speach_"+(i+1)+"\" id=\"speach\" \">";
                form += "</div>";
            }
            out.print(form);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @RequestMapping(value="/addScene")
    public String addScene(Model model, HttpServletRequest request){
        try {
            Scene scene = new Scene();
            scene.setIdFilm(Integer.parseInt(request.getParameter("idFilm")));
            scene.setIdType(Integer.parseInt(request.getParameter("type")));
            scene.setIdPlateau(Integer.parseInt(request.getParameter("plateau")));
            scene.setDuree(Time.valueOf(request.getParameter("duree")+":00"));
            scene.create(null);
            
            scene = (Scene)scene.find(null).get(0);
            int nombre = Integer.parseInt(request.getParameter("nombre"));
            for (int i = 0; i < nombre; i++) {
                PersonnageAction personnageAction = new PersonnageAction();
                personnageAction.setIdScene(scene.getId());
                personnageAction.setIdActeur(Integer.parseInt(request.getParameter("acteur_"+(i+1))));
                personnageAction.setIdEmotion(Integer.parseInt(request.getParameter("emotion_"+(i+1))));
                personnageAction.setPhrase(request.getParameter("mot_"+(i+1)));
                personnageAction.setDuree(Time.valueOf(request.getParameter("speach_"+(i+1))+":00"));
                personnageAction.create(null);
            }
            index(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }
    
    @RequestMapping(value="/recherche")
    public String recherche(HttpServletRequest request,Model model) throws Exception{
        ArrayList list = null;
        DaoGeneric daogeneric = new DaoGeneric();
        ArrayList listgenre = null;
        Vector genre = new Vector();
        try {
            String motcle = request.getParameter("motcle");
            list = daogeneric.find("select * from film where lower(titre) like lower('%"+motcle+"%') order by id asc", new Film());
            for(int i = 0 ; i < list.size() ; i++){
                listgenre = daogeneric.find("select * from genrefilm where id = "+((Film) list.get(i)).getId(), new Genrefilm());
                String aa = ((Genrefilm) listgenre.get(0)).getIntitule();
                for(int j = 1 ; j < listgenre.size() ; j++){
                    aa+=","+((Genrefilm) listgenre.get(j)).getIntitule();
                }
                genre.add(aa);  
            }
            model.addAttribute("recherche","recherche");
            model.addAttribute("nbrPage",new Pagination().getNbrPage());
            model.addAttribute("list",list);
            model.addAttribute("genre",genre);
        } catch (Exception e) {
            throw e;
        }
        return "index";
    }
    
    @RequestMapping(value="/parPage")
    public String parPage(Model model, HttpServletRequest request){
        try {
            Pagination pagination = new Pagination();
            pagination.setIdObjet(1);
            pagination.setPagination(Integer.parseInt(request.getParameter("parPage")));
            pagination.update(null);
            index(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }
    
    @RequestMapping(value="/pagination")
    public String pagination(Model model, HttpServletRequest request){
        int num = Integer.parseInt(request.getParameter("num"));
        DaoGeneric daogeneric = new DaoGeneric();
        ArrayList list = null;
        ArrayList listgenre = null;
        Vector genre = new Vector();
        Pagination page = new Pagination();
        try {
            Pagination pagination = (Pagination)page.find(null).get(0);
            num = pagination.getPagination()*num;
            String aa="";
            list = daogeneric.find("select * from film order by id asc offset "+num+" limit "+pagination.getPagination(), new Film());
            for(int i = 0 ; i < list.size() ; i++){
                listgenre = daogeneric.find("select * from genrefilm where id = "+((Film) list.get(i)).getId(), new Genrefilm());
                if(listgenre.size()!=0){
                   aa = ((Genrefilm) listgenre.get(0)).getIntitule();
                }
                
                for(int j = 1 ; j < listgenre.size() ; j++){
                    aa+=","+((Genrefilm) listgenre.get(j)).getIntitule();
                }
                genre.add(aa);
            }
            model.addAttribute("nbrPage",page.getNbrPage());
            model.addAttribute("list",list);
            model.addAttribute("genre",genre);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }
    
    @RequestMapping(value="/details")
    public String getDetails (Model model,HttpServletRequest request) throws Exception{
        int idFilm = Integer.parseInt(request.getParameter("idFilm"));
        ArrayList list = null;
        DaoGeneric daogeneric = new DaoGeneric();
        try{
            list = daogeneric.find("select * from vi_scene where idfilm = "+idFilm, new Vi_scene());
            model.addAttribute("details",list);
            Film film = new Film();
            film.setId(idFilm);
            model.addAttribute("film",film.find(null).get(0));
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "details";
    }
    
    @RequestMapping(value = "/planning")
    public String getPLanning(Model model,HttpServletRequest req) {
        try {
            int jour = Integer.parseInt(req.getParameter("jour"));
            int idfilm = Integer.parseInt(req.getParameter("idfilm"));
            GeneriqueDAO dao =new GeneriqueDAO();
            Connection con=new ConnectionDB().getConnection("postgres");
            Planning pl=new Planning();
            pl.setIdfilm(idfilm);
            ArrayList<Planning>plan=pl.getPlanning(con, dao);
            ArrayList<Planning> planning=pl.getPlanningJour(plan, jour);
            double nbrJour=pl.getNombreJour(idfilm, con, dao);
            model.addAttribute("plan",planning);
            model.addAttribute("nbrJour",nbrJour);
            model.addAttribute("idfilm",idfilm);
            return "Planning";
        } catch (Exception ex) {
            Logger.getLogger(AllController.class.getName()).log(Level.SEVERE, null, ex);
        }
         return "Planning";
    }
}
